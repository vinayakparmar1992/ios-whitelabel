//
// Created by Zeneil Ambekar on 18/12/14.
// Copyright (c) 2014 ZVA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZdpManagedCollectionObject.h"

@interface Shard : ZdpManagedCollectionObject

@property (nonatomic, strong) NSString *shard;
@property (nonatomic, strong) NSNumber *shard_count;
@property (nonatomic, strong) NSDate *shard_date;

- (instancetype)initWithShard:(NSString *)shard andShardCount:(NSNumber *)shard_count andShardDate:(NSDate *)shard_date;

+ (instancetype)shardWithShard:(NSString *)shard andShardCount:(NSNumber *)shard_count andShardDate:(NSDate *)shard_date;



@end