//
// Created by Zeneil Ambekar on 18/12/14.
// Copyright (c) 2014 ZVA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZdpManagedCollection.h"


@interface Shards : ZdpManagedCollection

+ (instancetype) getInstance;

@end