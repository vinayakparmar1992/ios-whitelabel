//
// Created by Zeneil Ambekar on 18/12/14.
// Copyright (c) 2014 ZVA. All rights reserved.
//

#import "NSDictionary+JSONRep.h"


@implementation NSDictionary (JSONRep)
- (NSString *)JSONRepresentation {
    return [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:self
                                                                          options:0
                                                                            error:nil]
                                 encoding:NSUTF8StringEncoding];

}
@end