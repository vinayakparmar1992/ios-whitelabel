//
//  ViewController.h
//  ZDPWrapperAppTest
//
//  Created by Zeneil Ambekar on 18/12/14.
//  Copyright (c) 2014 ZVA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZdpCollectionAdapter.h"

@interface ViewController : UIViewController <ZdpTableViewRenderDelegate>


@end

