//
// Created by Zeneil Ambekar on 18/12/14.
// Copyright (c) 2014 ZVA. All rights reserved.
//

#import "Shards.h"
#import "Shard.h"


@implementation Shards {

}

- (instancetype)init {
    self = [super init];
    return self;
}

- (NSString *)getCollectionName {
    return @"shards";
}

- (id)getResourceInstance {
    return [[Shard alloc] init];
}


static Shards *____shards = nil;
+ (instancetype)getInstance {
    if(!____shards)
        ____shards = [[Shards alloc] init];
    return ____shards;
}

@end