//
//  AppDelegate.h
//  ZDPWrapperAppTest
//
//  Created by Zeneil Ambekar on 18/12/14.
//  Copyright (c) 2014 ZVA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MeteorClient.h>
@class MeteorClient;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property(nonatomic, strong) MeteorClient *meteorClient;

@end

