//
// Created by Zeneil Ambekar on 18/12/14.
// Copyright (c) 2014 ZVA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UITableView.h>

@class ZdpManagedCollection;
@class ZdpManagedCollectionObject;

@protocol ZdpTableViewRenderDelegate <NSObject>
- (UITableViewCell *) viewForObject:(ZdpManagedCollectionObject *)object inTableView:(UITableView *)view;
@end

@interface ZdpCollectionAdapter : NSObject <UITableViewDataSource> {
}

@property (strong, nonatomic) id<ZdpTableViewRenderDelegate> renderDelegate;
@property (assign, nonatomic) BOOL autoRefreshTableView;


+ (instancetype)adapterWithCollection:(ZdpManagedCollection *)aCollection
                         andTableView:(UITableView *)aTableView
                    andRenderDelegate:(id <ZdpTableViewRenderDelegate>)renderDelegate;

- (instancetype) initWithCollection:(ZdpManagedCollection *)aCollection
                       andTableView:(UITableView *)aTableView
                  andRenderDelegate:(id <ZdpTableViewRenderDelegate>)renderDelegate;

- (void) addObject: (ZdpManagedCollectionObject *)aMangedCollectionObject;
- (void) removeObject: (ZdpManagedCollectionObject *)aMangedCollectionObject;
- (void)changeObject: (ZdpManagedCollectionObject *)aMangedCollectionObject
      usingNewFields: (NSDictionary *) changedFields;

@end