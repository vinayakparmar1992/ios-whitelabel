//
// Created by Zeneil Ambekar on 18/12/14.
// Copyright (c) 2014 ZVA. All rights reserved.
//

#import "ZdpManagedCollection.h"
#import "ZdpCollectionAdapter.h"
#import "ZdpManagedCollectionObject.h"
#import <ObjectiveDDP/MeteorClient.h>
#import <MeteorClient+CollectionManager.h>
#import <objc/runtime.h>

@implementation ZdpManagedCollection {
    NSMutableDictionary *mObjects;
    NSMutableArray *mListeners;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        mObjects = [[NSMutableDictionary alloc] init];
        mListeners = [[NSMutableArray alloc] init];
        [MeteorClient registerCollection:self];
    }


    return self;
}


- (NSMutableArray *) mListeners{
    if(!mListeners)
        mListeners = [[NSMutableArray alloc] init];
    return mListeners;
}

- (NSArray *)find {
    return [mObjects allValues];
}

- (void)addAdapterListener:(ZdpCollectionAdapter *)adapter {
    [self.mListeners addObject:adapter];
}

- (void)removeAdapterListener:(ZdpCollectionAdapter *)adapter {
    [self.mListeners removeObject:adapter];
}

- (void)saveObject:(ZdpManagedCollectionObject *)object {



}


- (void)onUpdateReceived:(NSInteger) type forObject:(NSDictionary *)object {
    ZdpManagedCollectionObject *obj = [self inflate:object];

    switch(type){
        case kCollectionAdded:
            [mObjects setValue:obj
                        forKey:[object valueForKey:@"_id"]];
            for (int i = 0; i < mListeners.count; ++i)
                [((ZdpCollectionAdapter *) mListeners[(NSUInteger) i]) addObject:obj];
            break;

        case kCollectionRemoved:
            [mObjects removeObjectForKey:[object valueForKey:@"_id"]];
            for (int i = 0; i < mListeners.count; ++i)
                [((ZdpCollectionAdapter *) mListeners[(NSUInteger) i]) removeObject:obj];
            break;

        default:break;
    }

}

- (ZdpManagedCollectionObject *)inflate:(NSDictionary *)dictionary {
    ZdpManagedCollectionObject *zdp = [self getResourceInstance];

    NSArray *allProperties = [dictionary allKeys];
    for(NSString *propertyKey in allProperties){
        id object = [dictionary valueForKey:propertyKey];
        if ([object isKindOfClass:[NSDictionary class]]) {
            NSTimeInterval t_date =  [(NSNumber *)[object valueForKey:@"$date"] floatValue] / 1000;
            object = [NSDate dateWithTimeIntervalSince1970:t_date];
        }
        [zdp setValue:object
               forKey:propertyKey];
    }

    return zdp;
}

- (NSString *)getCollectionName {
    @throw [NSException exceptionWithName:@"LANG_EX"
                                   reason:@"Abstract Class has to be overloaded"
                                 userInfo:@{@"class": NSStringFromClass(self.class)}];

}

- (id)getResourceInstance {
    @throw [NSException exceptionWithName:@"LANG_EX"
                                   reason:@"Abstract Class has to be overloaded"
                                 userInfo:@{@"class": NSStringFromClass(self.class)}];


}


@end