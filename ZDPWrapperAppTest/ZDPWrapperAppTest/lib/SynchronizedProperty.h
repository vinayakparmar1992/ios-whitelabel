//
// Created by Zeneil Ambekar on 19/12/14.
// Copyright (c) 2014 ZVA. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface SynchronizedProperty : NSObject

@property (nonatomic, strong) NSString *key;
@property (nonatomic, strong) id value;
@property (nonatomic, strong) Class type;

- (instancetype)initWithKey:(NSString *)key value:(id)value type:(Class)type;

- (instancetype)initWithType:(Class)type key:(NSString *)key;

- (instancetype)initWithKey:(NSString *)key value:(id)value;

+ (instancetype)aPropertyWithKey:(NSString *)key value:(id)value;


+ (instancetype)aPropertyWithType:(Class)type key:(NSString *)key;


+ (instancetype)aPropertyWithKey:(NSString *)key value:(id)value type:(Class)type;


@end