//
// Created by Zeneil Ambekar on 19/12/14.
// Copyright (c) 2014 ZVA. All rights reserved.
//

#import "SynchronizedProperty.h"


@implementation SynchronizedProperty {

}

- (instancetype)initWithKey:(NSString *)key
                      value:(id)value
                       type:(Class)type {
    self = [super init];
    if (self) {
        self.key = key;
        self.value = value;
        self.type = type;
    }

    return self;
}

- (instancetype)initWithKey:(NSString *)key value:(id)value {
    self = [super init];
    if (self) {
        self.key = key;
        self.value = value;
    }

    return self;
}

+ (instancetype)aPropertyWithKey:(NSString *)key value:(id)value {
    return [[self alloc] initWithKey:key value:value];
}


- (instancetype)initWithType:(Class)type key:(NSString *)key {
    self = [super init];
    if (self) {
        self.type = type;
        self.key = key;
    }

    return self;
}

+ (instancetype)aPropertyWithType:(Class)type key:(NSString *)key {
    return [[self alloc] initWithType:type key:key];
}


+ (instancetype)aPropertyWithKey:(NSString *)key
                           value:(id)value
                            type:(Class)type {
    return [[self alloc] initWithKey:key value:value type:type];
}



@end