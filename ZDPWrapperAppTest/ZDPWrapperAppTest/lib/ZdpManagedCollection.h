//
// Created by Zeneil Ambekar on 18/12/14.
// Copyright (c) 2014 ZVA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ObjectiveDDP/MeteorClient+CollectionManager.h>

@class ZdpCollectionAdapter;
@class ZdpManagedCollectionObject;

@interface ZdpManagedCollection : NSObject <ZdpCollectionDelegate>


- (instancetype)init;

- (NSArray *)find;
- (void)addAdapterListener:(ZdpCollectionAdapter *)adapter;
- (void)removeAdapterListener:(ZdpCollectionAdapter *)adapter;

- (void) saveObject: (ZdpManagedCollectionObject *)object;

@end