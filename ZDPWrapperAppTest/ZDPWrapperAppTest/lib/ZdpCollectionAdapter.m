//
// Created by Zeneil Ambekar on 18/12/14.
// Copyright (c) 2014 ZVA. All rights reserved.
//

#import "ZdpCollectionAdapter.h"
#import "ZdpManagedCollection.h"
#import "ZdpManagedCollectionObject.h"


@implementation ZdpCollectionAdapter {
    NSMutableArray *mSource;
    ZdpManagedCollection *mCollection;
    UITableView *mTableView;
}



- (instancetype) initWithCollection:(ZdpManagedCollection *)aCollection
                       andTableView:(UITableView *)aTableView
                  andRenderDelegate:(id <ZdpTableViewRenderDelegate>)renderDelegate {

    self = [super init];
    if (self) {
        mCollection = aCollection;
        mTableView = aTableView;
        mTableView.dataSource = self;
        mSource = [NSMutableArray arrayWithArray:[mCollection find]];
        _autoRefreshTableView = YES;
        [mCollection addAdapterListener: self];
        self.renderDelegate = renderDelegate;
    }

    return self;
}

+ (instancetype)adapterWithCollection:(ZdpManagedCollection *)aCollection
                           andTableView:(UITableView *)aTableView
                         andRenderDelegate:(id <ZdpTableViewRenderDelegate>)renderDelegate {
    return [[self alloc] initWithCollection:aCollection
                               andTableView:aTableView
                          andRenderDelegate:renderDelegate];
}

- (void)addObject:(ZdpManagedCollectionObject *)aMangedCollectionObject {
    [mSource addObject:aMangedCollectionObject];
    if (_autoRefreshTableView) [mTableView reloadData];
}

- (void)removeObject:(ZdpManagedCollectionObject *)aMangedCollectionObject {
    [mSource removeObject:aMangedCollectionObject];
    if (_autoRefreshTableView) [mTableView reloadData];
}

- (void)changeObject:(ZdpManagedCollectionObject *)aMangedCollectionObject
      usingNewFields:(NSDictionary *)changedFields {

    [((ZdpManagedCollectionObject *)mSource[[mSource indexOfObject:aMangedCollectionObject]]) update: changedFields];
    if (_autoRefreshTableView) [mTableView reloadData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return mSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [_renderDelegate viewForObject:mSource[(NSUInteger) indexPath.row]
                              inTableView:tableView];
}

@end