//
// Created by Zeneil Ambekar on 18/12/14.
// Copyright (c) 2014 ZVA. All rights reserved.
//

#import <ObjectiveDDP/MeteorClient.h>
#import "ZdpManagedCollectionObject.h"


@implementation ZdpManagedCollectionObject {

}
- (void)update:(NSDictionary *)dictionary {

}

- (NSUInteger)hash {
    return [__id hash];
}


- (BOOL)isEqual:(id)other {
    NSString *id = [other valueForKey:@"_id"];
    return id && [id isEqualToString:__id];
}


- (void)save {
    [[MeteorClient getInstance] callMethodName:@"insert"
                                    parameters:@[@"shards",[]] responseCallback:<#(MeteorClientMethodCallback)asyncCallback#>]
    [((AppDelegate *) [UIApplication sharedApplication].delegate).meteorClient
    callMethodName:@"insert"
    parameters:@[@"shards",[shard JSONRepresentation]]
    responseCallback:^(NSDictionary *response, NSError *error) {
        if(error)
            NSLog(@"Error: %@", error);
        else
            NSLog(@"Called: %@", [[response valueForKey:@"result"] valueForKey:@"response"]);
    }];
}

@end