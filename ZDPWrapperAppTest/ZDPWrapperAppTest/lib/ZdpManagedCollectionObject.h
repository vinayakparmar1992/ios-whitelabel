//
// Created by Zeneil Ambekar on 18/12/14.
// Copyright (c) 2014 ZVA. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface ZdpManagedCollectionObject : NSObject
@property (nonatomic, strong) NSString *_id;
@property (nonatomic, assign) NSNumber *timestamp;
- (void)update:(NSDictionary *)dictionary;

- (void)save;
@end