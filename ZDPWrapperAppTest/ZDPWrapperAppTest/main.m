//
//  main.m
//  ZDPWrapperAppTest
//
//  Created by Zeneil Ambekar on 18/12/14.
//  Copyright (c) 2014 ZVA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
