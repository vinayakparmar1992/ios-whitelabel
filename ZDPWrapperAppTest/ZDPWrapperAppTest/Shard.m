//
// Created by Zeneil Ambekar on 18/12/14.
// Copyright (c) 2014 ZVA. All rights reserved.
//

#import "Shard.h"


@implementation Shard {

}
- (instancetype)initWithShard:(NSString *)shard
                andShardCount:(NSNumber *)shard_count
                 andShardDate:(NSDate *)shard_date {
    self = [super init];
    if (self) {
        self.shard = shard;
        self.shard_count = shard_count;
        self.shard_date = shard_date;
    }

    return self;
}

+ (instancetype)shardWithShard:(NSString *)shard andShardCount:(NSNumber *)shard_count andShardDate:(NSDate *)shard_date {
    return [[self alloc] initWithShard:shard andShardCount:shard_count andShardDate:shard_date];
}


@end