 //
//  ViewController.m
//  ZDPWrapperAppTest
//
//  Created by Zeneil Ambekar on 18/12/14.
//  Copyright (c) 2014 ZVA. All rights reserved.
//

#import "ViewController.h"
#import "AppDelegate.h"
#import "NSDictionary+JSONRep.h"
#import "ZdpCollectionAdapter.h"
#import "Shards.h"
#import "Shard.h"

@interface ViewController () <UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tblTable;
@property (nonatomic, strong) ZdpCollectionAdapter *adapterDelegate;
@end

@implementation ViewController
- (IBAction)subscribe:(id)sender {
    [((AppDelegate *) [UIApplication sharedApplication].delegate).meteorClient addSubscription:@"all_shards"
                                                                                withParameters:@[@0, @5]];
}

- (IBAction)addToCollection:(id)sender {
    
//    [((AppDelegate *) [UIApplication sharedApplication].delegate).meteorClient
//            callMethodName:@"/shards/insert"
//                parameters:parameters
//          responseCallback:^(NSDictionary *response, NSError *error) {
//
//          }];

    Shard *xshard =  [[Shard alloc] initWithShard:@"Hello"
                                   andShardCount:@3
                                    andShardDate:[[NSDate alloc] init]];

    [xshard save];

    NSDictionary * shard = @{@"shard": @"from_ios"};

}

- (IBAction)pingServer:(id)sender {
    
    [((AppDelegate *) [UIApplication sharedApplication].delegate).meteorClient
            callMethodName:@"pingMe"
                parameters:@[]
          responseCallback:^(NSDictionary *response, NSError *error) {

              if(error)
                  NSLog(@"Error: %@", error);
              else
                  NSLog(@"Called");

          }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    _adapterDelegate = [[ZdpCollectionAdapter alloc] initWithCollection:[Shards getInstance]
                                                           andTableView:_tblTable
                                                      andRenderDelegate:self];
    //[self.tblTable setDelegate:self];
    [self subscribe:self];
}

- (void)viewWillDisappear:(BOOL)animated {

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UITableViewCell *)viewForObject:(ZdpManagedCollectionObject *)object inTableView:(UITableView *)view {
    static NSString *CellIdentifier = @"txtCell";

    UITableViewCell *cell = [view dequeueReusableCellWithIdentifier:CellIdentifier];

    if (!cell){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:CellIdentifier];

        
    }
    [cell.textLabel setText:((Shard *) object).shard];

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%d",indexPath.row);
}

@end
