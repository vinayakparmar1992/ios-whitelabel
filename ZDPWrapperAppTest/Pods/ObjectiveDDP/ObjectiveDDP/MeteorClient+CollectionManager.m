//
//  CollectionManager.m
//  Pods
//
//  Created by Zeneil Ambekar on 18/12/14.
//
//

#import "MeteorClient+CollectionManager.h"

//NSString *const kCollectionAdded = @"COLL_ADDED";
//NSString *const kCollectionRemoved = @"COLL_REMOVED";
//NSString *const kCollectionChanged = @"COLL_CHANGED";

@implementation MeteorClient (CollectionManager)



+ (BOOL)sendCollectionUpdate:(NSString *)collectionName forObject:(NSDictionary *)object ofType:(NSInteger)type {
    id<ZdpCollectionDelegate> collection = [[MeteorClient collectionRegistry] valueForKey:collectionName];
    if(collection){
        [collection onUpdateReceived:type
                           forObject:object];
        return YES;
    }
    return NO;
}

+ (void)registerCollection:(id <ZdpCollectionDelegate>)aCollection {
    [[MeteorClient collectionRegistry] setValue:aCollection
                          forKey:[aCollection getCollectionName]];
}

+ (void)revokeCollection:(id <ZdpCollectionDelegate>)aCollection{
    [[MeteorClient collectionRegistry] removeObjectForKey:[aCollection getCollectionName]];
}

static NSMutableDictionary *____collectionRegistry;
+(NSMutableDictionary *) collectionRegistry {
    if (____collectionRegistry==nil) {
        ____collectionRegistry = [[NSMutableDictionary alloc] init];
    }
    return ____collectionRegistry;
}

@end
