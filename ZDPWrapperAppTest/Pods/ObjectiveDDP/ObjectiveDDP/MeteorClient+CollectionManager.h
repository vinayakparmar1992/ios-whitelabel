//
//  CollectionManager.h
//  Pods
//
//  Created by Zeneil Ambekar on 18/12/14.
//
//

#import <Foundation/Foundation.h>
#import "MeteorClient.h"


enum {
    kCollectionAdded,
    kCollectionRemoved,
    kCollectionChanged
};

@protocol ZdpCollectionDelegate <NSObject>
- (NSString *) getCollectionName;
- (id) getResourceInstance;
@required

- (void) onUpdateReceived: (NSInteger) type
                forObject: (NSDictionary *) object;

@end

@interface MeteorClient (CollectionManager)

+ (void)  revokeCollection:(id <ZdpCollectionDelegate>)aCollection;

+ (BOOL) sendCollectionUpdate:(NSString *) collectionName
                    forObject: (NSDictionary *) object
                       ofType:(NSInteger) type;
+ (void)registerCollection:(id <ZdpCollectionDelegate>)aCollection;

@end

//FOUNDATION_EXPORT NSString *const kCollectionAdded;
//FOUNDATION_EXPORT NSString *const kCollectionRemoved;
//FOUNDATION_EXPORT NSString *const kCollectionChanged;
